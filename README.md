# lscripts



A collection of scripts that are useful for various tasks in GNU/Linux systems. They might also work on similar systems but this is not tested.

Currently there is only a small number of scripts and even these lack in functionality and documentation, but this should both change soon.

## Usage

I recommend cloning the project to a location of your choice and then adding the *scripts* directory to your PATH. This way you can update the project from the repository at any time and run the scripts from any location in your filesystem.

Of course you can also move the scripts to a location that's already in your PATH or seems more elegant in your PATH. This breaks the possibility of easy updates from the repository. To have the best of both worlds, you could consider symlinking the *scripts* directory (or the single scripts, which seems tedious).
